//
//  RouteCell.swift
//  TestLemiApp
//
//  Created by Mario Juni Jr, on 2/21/20.
//  Copyright © 2020 VeritasPay Inc. All rights reserved.
//

import UIKit

class FinchStopCell: UITableViewCell {

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var subtitleLabel: UILabel!
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
  
  func configure(route: FinchRoute){
    titleLabel.text = route.name
    subtitleLabel.text = route.groupId
  }

}
