//
//  RouteViewController.swift
//  TestLemiApp
//
//  Created by Mario Juni Jr, on 2/20/20.
//  Copyright © 2020 VeritasPay Inc. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class FinchStopViewController: UIViewController {
  
  @IBOutlet weak var agencyLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  var finchStop: FinchStop!
  private var finchRoute: FinchRoute?
  private let disposeBag = DisposeBag()
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.title = finchStop.name
    self.agencyLabel.text = finchStop.agency
    
    tableView.delegate = self
    tableView.dataSource = self
    tableView.tableFooterView = UIView(frame: CGRect.zero)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     if segue.identifier == "navigateToRoute" {
       if let destinationVC = segue.destination as? RouteTableViewController {
         destinationVC.finchRoute = finchRoute!
       }
     }
   }
   
}

extension FinchStopViewController : UITableViewDataSource{
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return finchStop!.routes!.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell =  tableView.dequeueReusableCell(withIdentifier: String(describing: FinchStopCell.self), for: indexPath) as! FinchStopCell
      cell.configure(route: finchStop!.routes![indexPath.row])
      return cell
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
      return 1
  }
}

extension FinchStopViewController: UITableViewDelegate{
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.finchRoute = finchStop!.routes![indexPath.row]
    self.performSegue(withIdentifier: "navigateToRoute", sender: nil)
   }
}
