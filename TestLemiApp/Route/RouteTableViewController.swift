//
//  StopTableViewController.swift
//  TestLemiApp
//
//  Created by Mario Juni Jr, on 2/21/20.
//  Copyright © 2020 VeritasPay Inc. All rights reserved.
//

import UIKit

class RouteTableViewController: UITableViewController {
  
  
  @IBOutlet weak var groupIdLabel: UILabel!
  var finchRoute: FinchRoute!
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = finchRoute.name
    self.groupIdLabel.text = "Group ID: \(finchRoute.groupId!)"
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell =  tableView.dequeueReusableCell(withIdentifier: String(describing: RouteCell.self), for: indexPath) as! RouteCell
    cell.configure(stop: finchRoute.stopTimes![indexPath.row])
    return cell
  }
  
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return finchRoute.stopTimes!.count
  }
  
}
