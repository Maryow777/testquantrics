//
//  StopCell.swift
//  TestLemiApp
//
//  Created by Mario Juni Jr, on 2/21/20.
//  Copyright © 2020 VeritasPay Inc. All rights reserved.
//

import UIKit

class RouteCell: UITableViewCell {
  @IBOutlet weak var departureLabel: UILabel!
  
  @IBOutlet weak var timestampLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  func configure(stop : StopTime){
    titleLabel.text = stop.shape
    departureLabel.text = "Departure Time: \(stop.departureTime!)"
    timestampLabel.text = "Timestamp: \(stop.departureTimeStamp!)"
  }

}
