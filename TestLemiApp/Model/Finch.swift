 

import Foundation

struct Finch: Codable {
    var name: String?
    var uri: String?
    var stops: [FinchStop]?
    var time: Int?
}

struct FinchStop: Codable {
    var uri: String?
    var name: String?
    var agency: String?
    var routes: [FinchRoute]?
}

struct FinchRoute: Codable {
    let uri: String?
    var stopTimes: [StopTime]?
    let name: String?
    let groupId: String?
    
     enum CodingKeys: String, CodingKey {
        case uri
        case name
        case stopTimes = "stop_times"
        case groupId = "route_group_id"
      }
}

struct StopTime: Codable {
    var serviceId: Int?
    var departureTime: String?
    var departureTimeStamp: Int?
    var shape: String?
    
    private enum CodingKeys: String, CodingKey {
        case serviceId = "service_Id",
            departureTime = "departure_time",
            departureTimeStamp = "departure_timestamp",
            shape
      }
}
