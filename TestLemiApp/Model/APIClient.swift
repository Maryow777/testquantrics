//
//  ApiRouter.swift
//  TestLemiApp
//
//  Created by Mario Juni on 22/04/2019.
//  Copyright © 2019 VeritasPay Inc. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa
public struct APIClient {
    
    let sessionManager: SessionManager
    let backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .default)
    static let instance = APIClient()
 
    init() {
        sessionManager = SessionManager(
            configuration: URLSessionConfiguration.default
        )
    }
  
  func getAllStop() -> Observable<Finch?> {
    return request("https:myttc.ca/finch_station.json")
    .validate(statusCode: 200..<300)
    .rx.responseJSON()
    .subscribeOn(backgroundScheduler)
    .observeOn(MainScheduler.instance)
    .map({ data -> Finch? in
       
        let decoder = JSONDecoder()
        var decodedData: Finch?
        do {
            decodedData = try decoder.decode(Finch.self, from: data as! Data )
            
        } catch let error as NSError {
                print(error.localizedDescription)
        }
        return decodedData
        
    })
  }

}
