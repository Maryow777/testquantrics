//
//  SearchViewModel.swift
//  TestLemiApp
//
//  Created by Mario Juni on 22/04/2019.
//  Copyright © 2019 VeritasPay Inc. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import Alamofire

final class FinchViewModel : ViewModelType{
    struct Input {
  
    }
    struct Output {
        let results : Driver<Finch>
        let loading : Driver<Bool>
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let loading = activityIndicator.asDriver()
        let errorTracker = ErrorTracker()
      
      let results = APIClient.init().getAllStop()
                          .trackActivity(activityIndicator)
                          .trackError(errorTracker)
                          .asObservable()
        
      
      let mappedResults = results.filter { $0 != nil }.map { $0! }.asDriverOnErrorJustComplete()
        
        return Output(results: mappedResults,
                      loading: loading)
    }
    
}

