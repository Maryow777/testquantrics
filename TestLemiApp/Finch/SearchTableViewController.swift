//
//  CityTableViewController.swift
//  TestLemiApp
//
//  Created by Mario Juni on 22/04/2019.
//  Copyright © 2019 VeritasPay Inc. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class SearchTableViewController: UITableViewController {
  
  var viewModel  = FinchViewModel()
  lazy var sb  = UIStoryboard(name: "Main", bundle: nil)
  private let disposeBag = DisposeBag()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.dataSource = nil
    tableView.delegate = nil
    tableView.tableFooterView = UIView(frame: CGRect.zero)
    bindViewModel()
    
  }
  
  func bindViewModel()  {
    let input = FinchViewModel.Input() // this is action only like search and or button
    let output = viewModel.transform(input: input)
    
    output.loading.drive(UIApplication.shared.rx.isNetworkActivityIndicatorVisible).disposed(by: disposeBag)
 
    
   output.results.map{$0.stops!}.drive(tableView.rx.items(cellIdentifier: String(describing: SearchCell.self),  cellType: SearchCell.self)){ (row ,element ,cell) in
          cell.configure(data: element)
    }.disposed(by: disposeBag)
    
    output.results.map{$0.name!}.drive(self.rx.title).disposed(by: disposeBag)
    tableView.rx.modelSelected(FinchStop.self).subscribe {  fench in
        
    }.disposed(by: disposeBag)
    
  
  }
}

