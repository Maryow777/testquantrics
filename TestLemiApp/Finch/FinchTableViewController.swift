//
//  CityTableViewController.swift
//  TestLemiApp
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class FinchTableViewController: UITableViewController {
  
  var viewModel  = FinchViewModel()
  lazy var sb  = UIStoryboard(name: "Main", bundle: nil)
  private let disposeBag = DisposeBag()
  
  @IBOutlet weak var dateLabel: UILabel!
  private var finchStop: FinchStop?
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.dataSource = nil
    tableView.delegate = nil
    tableView.tableFooterView = UIView(frame: CGRect.zero)
    bindViewModel()
    
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "navigateToFinchStop" {
      if let destinationVC = segue.destination as? FinchStopViewController {
        destinationVC.finchStop = finchStop!
      }
    }
  }
  
  
  func bindViewModel()  {
    let input = FinchViewModel.Input() // this is action only like search and or button
    let output = viewModel.transform(input: input)
    
    output.loading.drive(UIApplication.shared.rx.isNetworkActivityIndicatorVisible).disposed(by: disposeBag)
    
    
    output.results.map{$0.stops!}.drive(tableView.rx.items(cellIdentifier: String(describing: FinchCell.self),  cellType: FinchCell.self)){ (row ,element ,cell) in
      cell.configure(data: element)
    }.disposed(by: disposeBag)
    
    output.results.map{$0.name!}.drive(self.rx.title).disposed(by: disposeBag)
    tableView.rx.modelSelected(FinchStop.self).subscribe {  finch in
      self.finchStop = finch.element
      self.performSegue(withIdentifier: "navigateToFinchStop", sender: nil)
    }.disposed(by: disposeBag)
    
    output.results.map{$0.time!}.map(String.init).map{"Timestamp: \($0)"}.drive(dateLabel.rx.text).disposed(by: disposeBag)
  }
}

